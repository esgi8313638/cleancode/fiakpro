
# Projet FiakPro

## Prérequis
### Basic
- Se déplacer dans le répertoire du projet :
  ```sh
  cd existing_repo
  ```
- Installer les dépendances :
  ```sh
  composer install
  ```
- Configurer le fichier `.env.local` en vous basant sur le fichier `.env` pour paramétrer les variables d'environnement spécifiques à votre environnement local.

- Générer des clés JWT et ajouter une clé privée et une clé publique (.pem) dans le dossier `/config/jwt` avec la commande:
  ```sh
  php bin/console lexik:jwt:generate-keypair
  ```

### Extensions PHP

#### Activer l'Extension Sodium pour PHP
Ce projet nécessite que l'extension Sodium soit activée pour le PHP utilisé par Composer.

##### Localisez votre fichier php.ini utilisé par PHP CLI:
- Exécutez `php --ini` dans votre terminal pour voir le chemin du fichier `php.ini` utilisé par la ligne de commande.

##### Modifiez le fichier php.ini:
- Ouvrez le fichier `php.ini` indiqué.
- Trouvez la ligne `;extension=sodium` et supprimez le point-virgule (`;`) pour activer l'extension.
- Fermez et rouvrez votre terminal pour que les modifications prennent effet, ou redémarrez votre serveur web si nécessaire.

##### Vérification de l'activation
- Pour vérifier que l'extension Sodium est activée, exécutez le suivant dans votre terminal :
   ```sh
   php -m | grep sodium
   ```

#### Activer l'Extension GD
- De la même manière que pour les autres extensions PHP, allez dans votre `php.ini` et décommentez la ligne `;extension=gd`.

## Configuration des bases de données
- Créez deux bases de données : une pour le développement et une pour les tests. La base de données de test devrait avoir le même nom que celle de développement, mais avec le suffixe `_test`.
- Exécutez les migrations sur la base de données principale :
  ```sh
  php bin/console doctrine:migrations:migrate
  ```
- Exportez la base de données principale et importez-la dans la base de données de test. Ceci garantit que les structures des deux bases sont identiques.

## Lancer l'API
- Se déplacer dans le répertoire du projet :
  ```sh
  cd existing_repo
  ```
- Démarrer le serveur local :
  ```sh
  symfony serve
  ```

## API Routes
- Accéder à la documentation de l'API à l'adresse : `<nomprojet>:<port>/api/docs`
- L'accès est public pour des questions de simplicité.

## Tests
Avant de lancer les tests, assurez-vous de suivre ces étapes pour préparer l'environnement de test :

1. **Créer un fichier `.env.test` adapté** pour configurer les variables d'environnement utilisées spécifiquement pendant les tests.

2. **Charger les fixtures** pour initialiser la base de données de test avec des données préconfigurées :
    ```sh
    php bin/console doctrine:fixtures:load
    ```
   Cela remplira la base de données avec des données nécessaires pour effectuer des tests représentatifs.

- Pour exécuter les tests, utilisez la commande suivante :
   ```sh
   php bin/phpunit
   ```