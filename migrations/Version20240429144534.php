<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240429144534 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE devis DROP FOREIGN KEY FK_8B27C52B4F3F5D');
        $this->addSql('DROP INDEX IDX_8B27C52B4F3F5D ON devis');
        $this->addSql('ALTER TABLE devis CHANGE cient_id_id client_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE devis ADD CONSTRAINT FK_8B27C52BDC2902E0 FOREIGN KEY (client_id_id) REFERENCES client (id)');
        $this->addSql('CREATE INDEX IDX_8B27C52BDC2902E0 ON devis (client_id_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE devis DROP FOREIGN KEY FK_8B27C52BDC2902E0');
        $this->addSql('DROP INDEX IDX_8B27C52BDC2902E0 ON devis');
        $this->addSql('ALTER TABLE devis CHANGE client_id_id cient_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE devis ADD CONSTRAINT FK_8B27C52B4F3F5D FOREIGN KEY (cient_id_id) REFERENCES client (id)');
        $this->addSql('CREATE INDEX IDX_8B27C52B4F3F5D ON devis (cient_id_id)');
    }
}
