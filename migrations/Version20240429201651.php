<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240429201651 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE detail_devis DROP FOREIGN KEY FK_F6D70E7269678373');
        $this->addSql('DROP INDEX UNIQ_F6D70E7269678373 ON detail_devis');
        $this->addSql('ALTER TABLE detail_devis CHANGE devis_id_id devis_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE detail_devis ADD CONSTRAINT FK_F6D70E7241DEFADA FOREIGN KEY (devis_id) REFERENCES devis (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F6D70E7241DEFADA ON detail_devis (devis_id)');
        $this->addSql('ALTER TABLE facture DROP FOREIGN KEY FK_FE866410DC2902E0');
        $this->addSql('ALTER TABLE facture DROP FOREIGN KEY FK_FE86641069678373');
        $this->addSql('DROP INDEX IDX_FE866410DC2902E0 ON facture');
        $this->addSql('DROP INDEX UNIQ_FE86641069678373 ON facture');
        $this->addSql('ALTER TABLE facture ADD client_id INT DEFAULT NULL, ADD devis_id INT DEFAULT NULL, DROP client_id_id, DROP devis_id_id');
        $this->addSql('ALTER TABLE facture ADD CONSTRAINT FK_FE86641019EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE facture ADD CONSTRAINT FK_FE86641041DEFADA FOREIGN KEY (devis_id) REFERENCES devis (id)');
        $this->addSql('CREATE INDEX IDX_FE86641019EB6921 ON facture (client_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FE86641041DEFADA ON facture (devis_id)');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CAED7016E0');
        $this->addSql('DROP INDEX IDX_BF5476CAED7016E0 ON notification');
        $this->addSql('ALTER TABLE notification CHANGE facture_id_id facture_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CA7F2DEE08 FOREIGN KEY (facture_id) REFERENCES facture (id)');
        $this->addSql('CREATE INDEX IDX_BF5476CA7F2DEE08 ON notification (facture_id)');
        $this->addSql('ALTER TABLE paiement DROP FOREIGN KEY FK_B1DC7A1EED7016E0');
        $this->addSql('DROP INDEX UNIQ_B1DC7A1EED7016E0 ON paiement');
        $this->addSql('ALTER TABLE paiement CHANGE facture_id_id facture_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE paiement ADD CONSTRAINT FK_B1DC7A1E7F2DEE08 FOREIGN KEY (facture_id) REFERENCES facture (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B1DC7A1E7F2DEE08 ON paiement (facture_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE detail_devis DROP FOREIGN KEY FK_F6D70E7241DEFADA');
        $this->addSql('DROP INDEX UNIQ_F6D70E7241DEFADA ON detail_devis');
        $this->addSql('ALTER TABLE detail_devis CHANGE devis_id devis_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE detail_devis ADD CONSTRAINT FK_F6D70E7269678373 FOREIGN KEY (devis_id_id) REFERENCES devis (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F6D70E7269678373 ON detail_devis (devis_id_id)');
        $this->addSql('ALTER TABLE facture DROP FOREIGN KEY FK_FE86641019EB6921');
        $this->addSql('ALTER TABLE facture DROP FOREIGN KEY FK_FE86641041DEFADA');
        $this->addSql('DROP INDEX IDX_FE86641019EB6921 ON facture');
        $this->addSql('DROP INDEX UNIQ_FE86641041DEFADA ON facture');
        $this->addSql('ALTER TABLE facture ADD client_id_id INT DEFAULT NULL, ADD devis_id_id INT DEFAULT NULL, DROP client_id, DROP devis_id');
        $this->addSql('ALTER TABLE facture ADD CONSTRAINT FK_FE866410DC2902E0 FOREIGN KEY (client_id_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE facture ADD CONSTRAINT FK_FE86641069678373 FOREIGN KEY (devis_id_id) REFERENCES devis (id)');
        $this->addSql('CREATE INDEX IDX_FE866410DC2902E0 ON facture (client_id_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FE86641069678373 ON facture (devis_id_id)');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CA7F2DEE08');
        $this->addSql('DROP INDEX IDX_BF5476CA7F2DEE08 ON notification');
        $this->addSql('ALTER TABLE notification CHANGE facture_id facture_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CAED7016E0 FOREIGN KEY (facture_id_id) REFERENCES facture (id)');
        $this->addSql('CREATE INDEX IDX_BF5476CAED7016E0 ON notification (facture_id_id)');
        $this->addSql('ALTER TABLE paiement DROP FOREIGN KEY FK_B1DC7A1E7F2DEE08');
        $this->addSql('DROP INDEX UNIQ_B1DC7A1E7F2DEE08 ON paiement');
        $this->addSql('ALTER TABLE paiement CHANGE facture_id facture_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE paiement ADD CONSTRAINT FK_B1DC7A1EED7016E0 FOREIGN KEY (facture_id_id) REFERENCES facture (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B1DC7A1EED7016E0 ON paiement (facture_id_id)');
    }
}
