<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240429072726 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE client (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, adresse VARCHAR(255) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, telephone VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE detail_devis (id INT AUTO_INCREMENT NOT NULL, devis_id_id INT DEFAULT NULL, quantite INT DEFAULT NULL, prix_unitaire DOUBLE PRECISION DEFAULT NULL, UNIQUE INDEX UNIQ_F6D70E7269678373 (devis_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE detail_devis_produit (detail_devis_id INT NOT NULL, produit_id INT NOT NULL, INDEX IDX_2BE86980B7001301 (detail_devis_id), INDEX IDX_2BE86980F347EFB (produit_id), PRIMARY KEY(detail_devis_id, produit_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE devis (id INT AUTO_INCREMENT NOT NULL, cient_id_id INT DEFAULT NULL, date_creation DATE NOT NULL, statut VARCHAR(255) NOT NULL, INDEX IDX_8B27C52B4F3F5D (cient_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE facture (id INT AUTO_INCREMENT NOT NULL, client_id_id INT DEFAULT NULL, devis_id_id INT DEFAULT NULL, date_creation DATE DEFAULT NULL, paiement_ok TINYINT(1) NOT NULL, INDEX IDX_FE866410DC2902E0 (client_id_id), UNIQUE INDEX UNIQ_FE86641069678373 (devis_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notification (id INT AUTO_INCREMENT NOT NULL, facture_id_id INT DEFAULT NULL, motif VARCHAR(255) DEFAULT NULL, quantite_relance INT DEFAULT NULL, INDEX IDX_BF5476CAED7016E0 (facture_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE paiement (id INT AUTO_INCREMENT NOT NULL, facture_id_id INT DEFAULT NULL, montant DOUBLE PRECISION DEFAULT NULL, date_paiement DATE DEFAULT NULL, UNIQUE INDEX UNIQ_B1DC7A1EED7016E0 (facture_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE produit (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, prix DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE detail_devis ADD CONSTRAINT FK_F6D70E7269678373 FOREIGN KEY (devis_id_id) REFERENCES devis (id)');
        $this->addSql('ALTER TABLE detail_devis_produit ADD CONSTRAINT FK_2BE86980B7001301 FOREIGN KEY (detail_devis_id) REFERENCES detail_devis (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE detail_devis_produit ADD CONSTRAINT FK_2BE86980F347EFB FOREIGN KEY (produit_id) REFERENCES produit (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE devis ADD CONSTRAINT FK_8B27C52B4F3F5D FOREIGN KEY (cient_id_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE facture ADD CONSTRAINT FK_FE866410DC2902E0 FOREIGN KEY (client_id_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE facture ADD CONSTRAINT FK_FE86641069678373 FOREIGN KEY (devis_id_id) REFERENCES devis (id)');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CAED7016E0 FOREIGN KEY (facture_id_id) REFERENCES facture (id)');
        $this->addSql('ALTER TABLE paiement ADD CONSTRAINT FK_B1DC7A1EED7016E0 FOREIGN KEY (facture_id_id) REFERENCES facture (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE detail_devis DROP FOREIGN KEY FK_F6D70E7269678373');
        $this->addSql('ALTER TABLE detail_devis_produit DROP FOREIGN KEY FK_2BE86980B7001301');
        $this->addSql('ALTER TABLE detail_devis_produit DROP FOREIGN KEY FK_2BE86980F347EFB');
        $this->addSql('ALTER TABLE devis DROP FOREIGN KEY FK_8B27C52B4F3F5D');
        $this->addSql('ALTER TABLE facture DROP FOREIGN KEY FK_FE866410DC2902E0');
        $this->addSql('ALTER TABLE facture DROP FOREIGN KEY FK_FE86641069678373');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CAED7016E0');
        $this->addSql('ALTER TABLE paiement DROP FOREIGN KEY FK_B1DC7A1EED7016E0');
        $this->addSql('DROP TABLE client');
        $this->addSql('DROP TABLE detail_devis');
        $this->addSql('DROP TABLE detail_devis_produit');
        $this->addSql('DROP TABLE devis');
        $this->addSql('DROP TABLE facture');
        $this->addSql('DROP TABLE notification');
        $this->addSql('DROP TABLE paiement');
        $this->addSql('DROP TABLE produit');
    }
}
