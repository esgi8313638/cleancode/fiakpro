<?php

namespace App\DataFixtures;

use App\Domain\Entities\Client;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppClientFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $client = new Client();
        $client->setNom('Valentin');
        $client->setAdresse('30 rue de la fourberie');
        $client->setEmail('valentin@email.com');
        $client->setTelephone('0102030405');
        $manager->persist($client);
        $manager->flush();

        // Ajouter une référence
        $this->addReference('client-valentin', $client);

        $client2 = new Client();
        $client2->setNom('Baptiste');
        $client2->setAdresse('30 rue de l\'escroquerie');
        $client2->setEmail('Baptiste@email.com');
        $client2->setTelephone('0102030405');
        $manager->persist($client2);
        $manager->flush();

        // Ajouter une référence
        $this->addReference('client-baptiste', $client2);

        $client3 = new Client();
        $client3->setNom('Rémi');
        $client3->setAdresse('30 rue du crack');
        $client3->setEmail('remi@email.com');
        $client3->setTelephone('0102030405');
        $manager->persist($client3);
        $manager->flush();

        // Ajouter une référence
        $this->addReference('client-remi', $client3);
    }
}
