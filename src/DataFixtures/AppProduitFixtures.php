<?php

namespace App\DataFixtures;

use App\Domain\Entities\Produit;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppProduitFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $product = new Produit();
        $product->setNom('Vente service 1');
        $product->setDescription('Description du service 1');
        $product->setPrix('600');
        $manager->persist($product);

        // Ajouter une référence
        $this->addReference('produit-1', $product);

        $product2 = new Produit();
        $product2->setNom('Vente service ');
        $product2->setDescription('Description du service 2');
        $product2->setPrix('8000');
        $manager->persist($product2);

        // Ajouter une référence
        $this->addReference('produit-2', $product2);

        $product3 = new Produit();
        $product3->setNom('Vente service 3');
        $product3->setDescription('Description du service 3');
        $product3->setPrix('1200');
        $manager->persist($product3);

        // Ajouter une référence
        $this->addReference('produit-3', $product3);

        $manager->flush();
    }
}
