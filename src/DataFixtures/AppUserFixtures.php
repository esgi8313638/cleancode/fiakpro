<?php

namespace App\DataFixtures;

use App\Domain\Entities\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppUserFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // Replace with the one you created in your database.
        $hashedPasswordUser = '$2y$13$yA1ZgWiU9sleqT3ZE4ADiO7wC9FwnK6ee/N/FsSmx5puSk5fo4Kda';
        $user = new User();
        $user->setEmail('test@sfr.fr');
        $user->setPassword($hashedPasswordUser);
        $user->setRoles(['ROLE_USER']);
        $manager->persist($user);
        $manager->flush();
    }
}
