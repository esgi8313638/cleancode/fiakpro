<?php

namespace App\DataFixtures;

use App\Domain\Entities\DetailDevis;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class AppDetailDevisFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $detail1 = new DetailDevis();
        $detail1->setDevis($this->getReference('devis-valentin'));
        $detail1->addProduct($this->getReference('produit-1'));
        $detail1->setQuantite(2);
        $detail1->setPrixUnitaire(100.00);
        $manager->persist($detail1);

        $detail2 = new DetailDevis();
        $detail2->setDevis($this->getReference('devis-baptiste'));
        $detail2->addProduct($this->getReference('produit-2'));
        $detail2->setQuantite(1);
        $detail2->setPrixUnitaire(150.00);
        $manager->persist($detail2);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            AppDevisFixtures::class,
            AppProduitFixtures::class,
        ];
    }
}
