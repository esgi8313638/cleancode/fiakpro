<?php

namespace App\DataFixtures;

use App\Domain\Entities\Devis;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class AppDevisFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        // Devis pour Valentin
        $devis1 = new Devis();
        $devis1->setClient($this->getReference('client-valentin'));
        $devis1->setDateCreation(new DateTime('2024-04-29'));
        $devis1->setStatut('In Progress');
        $manager->persist($devis1);

        // Ajouter une référence
        $this->addReference('devis-valentin', $devis1);

        // Devis pour Baptiste
        $devis2 = new Devis();
        $devis2->setClient($this->getReference('client-baptiste'));
        $devis2->setDateCreation(new DateTime('2024-05-01'));
        $devis2->setStatut('Completed');
        $manager->persist($devis2);

        // Ajouter une référence
        $this->addReference('devis-baptiste', $devis2);

        // Devis pour Rémi
        $devis3 = new Devis();
        $devis3->setClient($this->getReference('client-remi'));
        $devis3->setDateCreation(new DateTime('2024-05-03'));
        $devis3->setStatut('Pending');
        $manager->persist($devis3);

        // Ajouter une référence
        $this->addReference('devis-remi', $devis3);

        // Flush all changes to the database
        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            AppClientFixtures::class, // Assurez-vous que les clients sont chargés en premier
        ];
    }
}
