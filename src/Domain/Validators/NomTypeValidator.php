<?php

// src/App/Domain/Validators/NomValidator.php
namespace App\Domain\Validators;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

#[\Attribute] class NomTypeValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof NomTypeValidator) {
            throw new UnexpectedTypeException($constraint, NomTypeValidator::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        // Ajoutez votre logique de validation pour le nom ici
        if (!preg_match('/^[a-zA-Z\-\'\s]+$/', $value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $value)
                ->addViolation();
        }
    }
}
