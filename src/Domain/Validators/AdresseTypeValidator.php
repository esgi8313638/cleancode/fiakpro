<?php

// src/App/Domain/Validators/AdresseValidator.php
namespace App\Domain\Validators;

use AppBundle\Validator\Constraints\Adresse;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

#[\Attribute] class AdresseTypeValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof Adresse) {
            throw new UnexpectedTypeException($constraint, Adresse::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        // Ajoutez votre logique de validation pour l'adresse ici
        if (!preg_match('/^[a-zA-Z0-9\s\-]+$/', $value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $value)
                ->addViolation();
        }
    }
}
