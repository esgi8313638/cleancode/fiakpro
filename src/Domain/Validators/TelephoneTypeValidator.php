<?php

// src/App\Domain\Validators/TelephoneValidator.php
namespace App\Domain\Validators;

use AppBundle\Validator\Constraints\Telephone;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

#[\Attribute] class TelephoneTypeValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof Telephone) {
            throw new UnexpectedTypeException($constraint, Telephone::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        // Ajoutez votre logique de validation pour le numéro de téléphone ici
        if (!preg_match('/^[0-9]{10}$/', $value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $value)
                ->addViolation();
        }
    }
}
