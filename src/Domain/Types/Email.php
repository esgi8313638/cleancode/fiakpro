<?php

// src/AppBundle/Validator/Constraints/EmailType.php
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_PROPERTY)]
class Email extends Constraint
{
    public $message = 'L\'adresse e-mail "{{ value }}" n\'est pas valide.';
}
