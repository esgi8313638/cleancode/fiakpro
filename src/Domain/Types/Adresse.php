<?php

// src/AppBundle/Validator/Constraints/Adresse.php
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_PROPERTY)]
class Adresse extends Constraint
{
    public $message = 'L\'adresse "{{ value }}" n\'est pas valide.';
}
