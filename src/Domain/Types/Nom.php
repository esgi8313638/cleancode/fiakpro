<?php

// src/AppBundle/Validator/Constraints/Nom.php
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_PROPERTY)]
class Nom extends Constraint
{
    public $message = 'Le nom "{{ value }}" n\'est pas valide.';
}
