<?php

// src/AppBundle/Validator/Constraints/Telephone.php
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_PROPERTY)]
class Telephone extends Constraint
{
    public $message = 'Le numéro de téléphone "{{ value }}" n\'est pas valide.';
}
