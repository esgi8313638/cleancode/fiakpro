<?php

namespace App\Domain\Entities;

use ApiPlatform\Metadata\ApiResource;
use App\Domain\Repositories\DevisRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\SerializedName;


#[ORM\Entity(repositoryClass: DevisRepository::class)]
#[ApiResource]
class Devis
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'devis')]
    private ?Client $client = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $date_creation = null;

    #[ORM\Column(length: 255)]
    private ?string $statut = null;

    #[ORM\OneToOne(mappedBy: 'devis', cascade: ['persist', 'remove'])]
    private ?DetailDevis $detailDevis = null;

    #[ORM\OneToOne(mappedBy: 'devis', cascade: ['persist', 'remove'])]
    private ?Facture $facture = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    #[SerializedName("client")]
    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    #[SerializedName("date_creation")]
    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->date_creation;
    }

    public function setDateCreation(?\DateTimeInterface $date_creation): self
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    #[SerializedName("detail_devis")]
    public function getDetailDevis(): ?DetailDevis
    {
        return $this->detailDevis;
    }

    public function setDetailDevis(?DetailDevis $detailDevis): self
    {
        // unset the owning side of the relation if necessary
        if ($detailDevis === null && $this->detailDevis !== null) {
            $this->detailDevis->setDevis(null);
        }

        // set the owning side of the relation if necessary
        if ($detailDevis !== null && $detailDevis->getDevis() !== $this) {
            $detailDevis->setDevis($this);
        }

        $this->detailDevis = $detailDevis;

        return $this;
    }

    public function getFacture(): ?Facture
    {
        return $this->facture;
    }

    public function setFacture(?Facture $facture): self
    {
        // unset the owning side of the relation if necessary
        if ($facture === null && $this->facture !== null) {
            $this->facture->setDevis(null);
        }

        // set the owning side of the relation if necessary
        if ($facture !== null && $facture->getDevis() !== $this) {
            $facture->setDevis($this);
        }

        $this->facture = $facture;

        return $this;
    }
}
