<?php

namespace App\Domain\Entities;

use ApiPlatform\Metadata\ApiResource;
use App\Domain\Repositories\ProduitRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\SerializedName;


#[ORM\Entity(repositoryClass: ProduitRepository::class)]
#[ApiResource]
class Produit
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(nullable: true)]
    private ?float $prix = null;

    /**
     * @var Collection<int, DetailDevis>
     */
    #[ORM\ManyToMany(targetEntity: DetailDevis::class, mappedBy: 'product')]
    private Collection $detailDevis;

    public function __construct()
    {
        $this->detailDevis = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(?float $prix): static
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * @return Collection<int, DetailDevis>
     */
    #[SerializedName("detail_devis")]
    public function getDetailDevis(): Collection
    {
        return $this->detailDevis;
    }

    public function addDetailDevi(DetailDevis $detailDevi): static
    {
        if (!$this->detailDevis->contains($detailDevi)) {
            $this->detailDevis->add($detailDevi);
            $detailDevi->addProduct($this);
        }

        return $this;
    }

    public function removeDetailDevi(DetailDevis $detailDevi): static
    {
        if ($this->detailDevis->removeElement($detailDevi)) {
            $detailDevi->removeProduct($this);
        }

        return $this;
    }
}
