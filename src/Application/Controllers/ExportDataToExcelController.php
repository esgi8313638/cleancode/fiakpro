<?php

namespace App\Application\Controllers;

use App\Application\Services\ExcelGeneratorService;
use App\Application\Services\ZipCreatorService;
use App\Domain\Entities\Facture;
use App\Domain\Repositories\FactureRepository;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;

class ExportDataToExcelController extends AbstractController implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    private ExcelGeneratorService $excelGeneratorService;
    private ZipCreatorService $zipCreatorService;

    public function __construct(ExcelGeneratorService $excelGeneratorService, ZipCreatorService $zipCreatorService) {
        $this->excelGeneratorService = $excelGeneratorService;
        $this->zipCreatorService = $zipCreatorService;
    }

    /**
     * Contrôleur pour l'exportation des factures.
     * Cette méthode s'occupe de récupérer toutes les factures, de les préparer pour l'exportation
     * et finalement de générer un fichier Excel contenant les données des factures valides
     * ainsi qu'un rapport des erreurs pour les factures avec des données manquantes ou incorrectes.
     *
     * @param FactureRepository $factureRepository Le repository permettant l'accès aux données des factures.
     * @return StreamedResponse Une réponse streamée contenant le fichier Excel généré, prêt à être téléchargé par l'utilisateur.
     */
    #[Route("/api/export/factures", name: "export_factures", methods: ['GET'])]
    public function exportFactures(FactureRepository $factureRepository): StreamedResponse
    {
        // Définition initiale du nom du fichier à générer.
        $fileName = 'factures';
        // Chemin d'accès au template Excel utilisé pour l'exportation.
        $templatePath = $this->getProjectDir() . '/templates/exports/template_factures_export.xlsx';

        // Récupération de la liste des factures.
        $factures = $factureRepository->findAll();

        // Si aucunes facture enregistrée, renvoi un message d'information.
        if (empty($factures)) {
            return new StreamedResponse(function () {
                echo "Aucune donnée à exporter.";
            }, 200);
        }

        // Préparation des données pour l'exportation, incluant la vérification des données de chaque facture.
        $exportData = $this->prepareExportData($factures);

        // Génération du fichier Excel basé sur les données préparées et les erreurs collectées, puis renvoi du fichier comme réponse streamée.
        return $this->createAndStreamExcelFile($exportData['data'], $fileName, $templatePath, $exportData['errors']);
    }

    protected function getProjectDir(): string
    {
        return $this->getParameter('kernel.project_dir');
    }

    /**
     * Prépare les données pour l'exportation en traitant chaque facture fournie.
     * Cette fonction passe en revue la liste des factures, vérifie chaque facture pour détecter d'éventuelles erreurs
     * de données et prépare une ligne de données formatée pour l'exportation. Les erreurs détectées sont collectées et associées
     * à la ligne correspondante pour faciliter le suivi et la correction.
     *
     * @param array $factures Tableau des objets Facture à exporter.
     * @return array Un tableau contenant deux clés : 'data' avec les lignes de données préparées pour l'exportation,
     *               et 'errors' contenant les erreurs associées à certaines lignes, le cas échéant.
     */
    private function prepareExportData(array $factures): array
    {
        $data = []; // Initialisation du tableau qui contiendra les données formatées pour l'export.
        $listErrors = []; // Initialisation du tableau qui recueillera les erreurs trouvées.

        // Itération sur chaque facture pour traitement.
        foreach ($factures as $index => $facture) {
            // Vérification des données de la facture courante pour détecter les erreurs potentielles.
            $errorsCurrentLine = $this->checkDataForExport($facture);

            // Si des erreurs sont trouvées, elles sont ajoutées au tableau des erreurs avec une référence à la ligne concernée.
            if (!empty($errorsCurrentLine)) {
                $listErrors['Ligne ' . ($index + 1) . ' (facture ' . $facture->getId() . ')'] = $errorsCurrentLine;
            }

            // Si la facture passe la vérification, une ligne de données formatée est préparée pour l'export.
            $data[] = $this->formatRowForExport($facture);
        }

        // Renvoi du tableau contenant les données préparées et les erreurs collectées.
        return ['data' => $data, 'errors' => $listErrors];
    }

    /**
     * Vérifie la validité des données d'une facture avant exportation.
     * Cette méthode passe en revue les informations essentielles d'une facture et de ses données liées
     * pour s'assurer qu'elles sont complètes et conformes aux attentes. En cas de données manquantes
     * ou non conformes, un rapport d'erreurs est généré pour faciliter la correction.
     *
     * @param Facture $facture L'objet Facture à vérifier, contenant toutes les informations nécessaires à l'exportation.
     * @return array Un tableau contenant les messages d'erreur identifiés lors de la vérification. Le tableau est vide si aucune erreur n'est détectée.
     */
    public function checkDataForExport(Facture $facture): array
    {
        $errors = [];

        // Vérifie la présence de l'id de la facture.
        if (empty($facture->getId())) {
            $errors[] = "L'ID de la facture est manquant.";
        }

        // Assure que l'état du paiement de la facture est bien défini.
        if (null === $facture->isPaiementOk()) {
            $errors[] = "L'état du paiement de la facture est manquant.";
        }

        // Assure que la date de création de la facture est bien définie.
        if (null === $facture->getDateCreation()) {
            $errors[] = "La date de création de la facture est manquante.";
        }

        // Vérifications supplémentaires pour s'assurer que les informations du client sont complètes.
        $client = $facture->getClient();
        if (null === $client) {
            $errors[] = "Les informations du client sont manquantes.";
        } else {
            // Vérification des détails du client.
            if (empty($client->getNom())) {
                $errors[] = "Le nom du client est manquant.";
            }
        }

        // Vérifications supplémentaires pour s'assurer que le devis est bien présent.
        $devis = $facture->getDevis();
        if (null === $devis) {
            $errors[] = "Le devis est manquant.";
        }

        // Vérifications supplémentaires pour s'assurer que les informations du paiement sont complètes.
        $paiement = $facture->getPaiement();
        if ($paiement && !$paiement->getMontant()) {
            // Vérification du montant du paiement.
            if (empty($paiement->getMontant())) {
                $errors[] = "Le montant du paiement est manquant.";
            }
        }

        return $errors; // Retourne le tableau d'erreurs pour traitement ultérieur.
    }

    /**
     * Prépare et formatte une ligne de données pour l'exportation basée sur les informations d'une facture.
     * Cette méthode traite individuellement chaque facture pour générer une ligne de données adaptée à l'exportation Excel.
     *
     * @param Facture $facture L'entité Facture contenant les informations à exporter.
     * @return array La ligne de données formatée pour l'exportation.
     */
    private function formatRowForExport(Facture $facture): array
    {
        // Vérifier si le client, le devis et le paiement sont bien récupérés
        $client = $facture->getClient();
        if (null === $client) {
            return ["ERROR: Aucun client associé à la facture"];
        }

        $devis = $facture->getDevis();
        if (null === $devis) {
            return ["ERROR: Aucun devis associé à la facture"];
        }

        // Initialisation des données de la ligne avec des valeurs par défaut pour chaque colonne
        $rowData = [
            // Colonne A : Détermination de l'état du paiement de la facture pour l'export. 'OK' pour un paiement effectué, 'NON' pour un paiement impayé.
            $facture->isPaiementOk() ? 'OK' : 'NON',
            // Colonne B : Identifiant du client, 'NULL' si non renseigné.
            $client->getId() ?: 'NULL',
            // Colonne C : Identifiant du devis, 'NULL' si non renseigné.
            $devis->getId() ?: 'NULL',
            // Colonne D : Date de création de la facture formatée, 'NULL' si non renseignée.
            $facture->getDateCreation()?->format('d/m/Y') ?: 'NULL',
            // Colonne E : Nom du client, 'NULL' si non renseigné.
            $client->getNom() ?: 'NULL',
            // Colonne F : Montant du paiement, 'NULL' si non renseigné.
            $facture->getPaiement() && $facture->getPaiement()->getMontant() ? $facture->getPaiement()->getMontant() : 'NULL',
        ];

        return $rowData;
    }

    /**
     * Génère un fichier Excel à partir des données fournies et le transmet directement sans stockage intermédiaire.
     * Si des erreurs sont présentes, elles sont ajoutées dans un fichier séparé et tout est compressé dans un fichier ZIP.
     *
     * @param array $data Les données à insérer dans le fichier Excel.
     * @param string $fileName Le nom de base pour le fichier généré (sans extension).
     * @param string|null $templatePath Le chemin vers un fichier modèle Excel, s'il est utilisé.
     * @param array $listErrors Liste des erreurs associées aux données, s'il y en a.
     * @return StreamedResponse La réponse streamée qui déclenche le téléchargement du fichier Excel.
     */
    private function createAndStreamExcelFile(array $data, string $fileName, ?string $templatePath = null, array $listErrors = []): StreamedResponse
    {
        // Création de la réponse streamée sans stocker le contenu en mémoire serveur.
        $response = new StreamedResponse();

        // Service pour la création d'archives ZIP.
        $zipCreator = $this->zipCreatorService;

        $response->setCallback(function() use ($data, $templatePath, $listErrors, $fileName, $zipCreator) {
            // Création d'un chemin temporaire pour le fichier Excel généré.
            $tempExcelPath = tempnam(sys_get_temp_dir(), 'excel') . '.xlsx';
            // Génération du fichier Excel basée sur les données et le modèle fourni.
            $spreadsheet = $this->excelGeneratorService->createExcelSpreadsheet($data, $templatePath);
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
            $writer->save($tempExcelPath);

            // Préparation du rapport d'erreurs sous forme de fichier JSON si des erreurs sont présentes.
            $tempErrorPath = null;
            if (!empty($listErrors)) {
                $tempErrorPath = tempnam(sys_get_temp_dir(), 'errors') . '.json';
                $errorsContent = json_encode($listErrors, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
                file_put_contents($tempErrorPath, $errorsContent);
            }

            // Création d'une archive ZIP contenant le fichier Excel et, le cas échéant, le fichier d'erreurs.
            $zipPath = tempnam(sys_get_temp_dir(), 'export') . '.zip';
            $filesToZip = [$fileName . '_' . date('Y-m-d_H-i-s') . '.xlsx' => $tempExcelPath];
            if ($tempErrorPath) {
                $filesToZip['errors.json'] = $tempErrorPath; // Inclusion du fichier d'erreurs dans l'archive ZIP.
            }
            $zipCreator->createZipArchive($filesToZip, $zipPath);

            // Transmission du fichier ZIP au client.
            readfile($zipPath);

            // Nettoyage des fichiers temporaires créés durant le processus.
            unlink($tempExcelPath);
            if ($tempErrorPath) {
                unlink($tempErrorPath);
            }
            unlink($zipPath);
        });

        // Configuration des en-têtes HTTP pour le téléchargement du fichier ZIP.
        $response->headers->set('Content-Type', 'application/zip');
        $finalFileName = 'export_' . $fileName . '_' . date('Y-m-d_H-i-s') . '.zip';
        $response->headers->set('Content-Disposition', $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $finalFileName
        ));

        return $response; // Renvoie la réponse configurée pour le téléchargement.
    }
}