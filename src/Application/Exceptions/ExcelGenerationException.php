<?php

namespace App\Application\Exceptions;

/**
 * Base exception class for all Excel generation related errors.
 */
class ExcelGenerationException extends \Exception
{
    public function __construct($message = "An error occurred during Excel generation.", $code = 0, \Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}