<?php

namespace App\Application\Exceptions;

use App\Application\Exceptions\ExcelGenerationException;

/**
 * Exceptions for errors occurring during the loading of an Excel template.
 */
class ExcelTemplateLoadException extends ExcelGenerationException
{}