<?php

namespace App\Application\Exceptions;

use App\Application\Exceptions\ExcelGenerationException;

/**
 * Exceptions for errors related to data injection into an Excel spreadsheet.
 */
class ExcelDataInjectionException extends ExcelGenerationException
{}