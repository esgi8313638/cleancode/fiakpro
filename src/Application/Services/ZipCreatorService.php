<?php

namespace App\Application\Services;

use Exception;
use ZipArchive;
use Symfony\Component\Filesystem\Filesystem;

class ZipCreatorService
{
    private Filesystem $filesystem;

    public function __construct()
    {
        $this->filesystem = new Filesystem();
    }

    /**
     * Crée une archive ZIP et y inclut les fichiers fournis.
     *
     * @param array $files Associative array of file paths to include in the zip archive ['fileName' => 'filePath']
     * @param string $zipPath Chemin complet où l'archive ZIP sera enregistrée.
     * @throws Exception Si la création de l'archive ou l'ajout de fichiers échoue.
     */
    public function createZipArchive(array $files, string $zipPath): void
    {
        $zip = new ZipArchive();

        try {
            if ($zip->open($zipPath, ZipArchive::CREATE) !== TRUE) {
                throw new Exception("Impossible de créer l'archive ZIP : $zipPath");
            }

            foreach ($files as $fileName => $filePath) {
                if (!$this->filesystem->exists($filePath)) {
                    throw new Exception("Le fichier à inclure dans l'archive ZIP n'existe pas : $filePath");
                }

                if (!$zip->addFile($filePath, $fileName)) {
                    throw new Exception("Impossible d'ajouter le fichier $filePath à l'archive ZIP : $zipPath");
                }
            }
        } catch (Exception $e) {
            // En cas d'erreur, fermez l'archive et supprimez-la si nécessaire, puis relancez l'exception.
            if (isset($zip)) {
                $zip->close();
                if ($this->filesystem->exists($zipPath)) {
                    $this->filesystem->remove($zipPath);
                }
            }
            throw $e; // Relancer l'exception pour la gestion en amont.
        }

        // Assurez-vous de fermer l'archive si tout se passe bien.
        $zip->close();
    }
}