<?php

namespace App\Application\Services;

use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * Interface for a wrapper around PhpSpreadsheet IOFactory.
 *
 * This interface defines the contract for a service that allows loading
 * Excel spreadsheets from a file, facilitating the mocking
 * of this functionality in unit tests.
 */
interface IIOFactoryWrapper {
    /**
     * Loads an Excel spreadsheet from a file.
     *
     * @param string $fileName Path to the file to load.
     * @return Spreadsheet An instance of Spreadsheet loaded from the file.
     */
    public function load(string $fileName): Spreadsheet;
}