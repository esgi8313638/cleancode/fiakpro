<?php

namespace App\Application\Services;

use App\Application\Exceptions\ExcelDataInjectionException;
use App\Application\Exceptions\ExcelTemplateLoadException;
use Exception;
use App\Application\Services\IIOFactoryWrapper;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * Service for generating Excel spreadsheets.
 *
 * Provides functionalities to create Excel spreadsheets,
 * either from provided data or from an existing template.
 */
class ExcelGeneratorService implements LoggerAwareInterface
{
    use LoggerAwareTrait; // Integrates functionalities to use a logger.

    private IIOFactoryWrapper $ioFactoryWrapper;
    private FileSystem $fileSystem;

    /**
     * Constructor for the ExcelGeneratorService.
     *
     * @param IIOFactoryWrapper $ioFactoryWrapper A wrapper around PhpSpreadsheet IOFactory to facilitate mocking.
     * @param FileSystem $fileSystem The service for interactions with the file system.
     */
    public function __construct(IIOFactoryWrapper $ioFactoryWrapper, FileSystem $fileSystem) {
        $this->ioFactoryWrapper = $ioFactoryWrapper;
        $this->fileSystem = $fileSystem;
    }

    /**
     * Creates and returns an Excel spreadsheet based on the provided data and/or a template.
     *
     * @param array $data The data to be inserted into the spreadsheet.
     * @param string|null $templatePath The optional path to an Excel template.
     * @return Spreadsheet The generated Excel spreadsheet.
     * @throws ExcelTemplateLoadException If loading the template fails.
     * @throws ExcelDataInjectionException If data injection fails.
     */
    public function createExcelSpreadsheet(array $data, string $templatePath = null): Spreadsheet {
        if (empty($data)) {
            $this->logger->warning("The data provided for Excel generation is empty.");
            throw new ExcelDataInjectionException("The data provided for Excel generation is empty.");
        }

        try {
            $spreadsheet = $templatePath ? $this->loadTemplate($templatePath) : new Spreadsheet();
            $this->logger->debug("Excel sheet successfully created.", ['templatePath' => $templatePath]);
        } catch (Exception $e) {
            $this->logger->error("Error during the creation of the Excel sheet.", ['exception' => $e]);
            throw $e; // Propagates the exception for further handling.
        }

        $this->populateSpreadsheetWithData($spreadsheet, $data);

        return $spreadsheet;
    }

    /**
     * Loads an Excel spreadsheet from a specified template.
     *
     * @param string $path The path to the template file.
     * @return Spreadsheet The loaded spreadsheet.
     * @throws ExcelTemplateLoadException If the specified file does not exist or is not readable.
     */
    private function loadTemplate(string $path): Spreadsheet {
        if (!$this->fileSystem->fileExistsAndReadable($path)) {
            $this->logger->error("The specified template file does not exist or is not readable.", ['path' => $path]);
            throw new ExcelTemplateLoadException("The specified template file does not exist or is not readable: $path");
        }
        $this->logger->debug("Excel template successfully loaded.", ['path' => $path]);
        return $this->ioFactoryWrapper->load($path);
    }

    /**
     * Injects the provided data into the Excel spreadsheet.
     *
     * @param Spreadsheet $spreadsheet The Excel spreadsheet to populate.
     * @param array $data The data to insert into the spreadsheet.
     * @throws ExcelDataInjectionException If an error occurs during data injection.
     *
     * This method fills the spreadsheet with the provided data,
     * placing each data element in the corresponding cells.
     */
    private function populateSpreadsheetWithData(Spreadsheet $spreadsheet, array $data): void {
        try {
            $sheet = $spreadsheet->getActiveSheet();
            foreach ($data as $rowIndex => $row) {
                foreach ($row as $columnIndex => $value) {
                    $columnLetter = Coordinate::stringFromColumnIndex($columnIndex + 1);
                    $cellId = $columnLetter . ($rowIndex + 2); // Excel cell identifiers start at 1, not 0.
                    $sheet->setCellValue($cellId, $value);
                }
            }
            $this->logger->debug("Data successfully inserted into the Excel sheet.");
        } catch (\PhpOffice\PhpSpreadsheet\Exception $e) {
            // Log and throw a specific exception if an error occurs during data insertion.
            $this->logger->error("Failed to inject data into the Excel sheet.", ['exception' => $e]);
            throw new ExcelDataInjectionException('Failed to inject data into Excel spreadsheet: ' . $e->getMessage(), 0, $e);
        }
    }
}