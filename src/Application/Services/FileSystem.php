<?php

namespace App\Application\Services;

/**
 * The FileSystem service encapsulates file system interactions.
 *
 * This abstraction allows for greater flexibility in unit testing,
 * by enabling the mocking of file existence and readability checks.
 */
class FileSystem {
    /**
     * Checks if a file exists and is readable.
     *
     * @param string $path The path to the file to check.
     * @return bool Returns true if the file exists and is readable, false otherwise.
     */
    public function fileExistsAndReadable(string $path): bool {
        return file_exists($path) && is_readable($path);
    }
}