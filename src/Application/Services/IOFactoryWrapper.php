<?php

namespace App\Application\Services;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * Implementation of IIOFactoryWrapper using PhpSpreadsheet's IOFactory.
 *
 * This wrapper replaces static calls to IOFactory::load
 * with instance calls, which facilitates mocking in tests.
 */
class IOFactoryWrapper implements IIOFactoryWrapper
{
    /**
     * {@inheritdoc}
     */
    public function load(string $fileName): Spreadsheet
    {
        return IOFactory::load($fileName);
    }
}