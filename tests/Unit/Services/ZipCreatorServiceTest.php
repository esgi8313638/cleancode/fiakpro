<?php

namespace App\Tests\Unit\Services;

use App\Application\Services\ZipCreatorService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Filesystem\Filesystem;

class ZipCreatorServiceTest extends TestCase
{
    private ZipCreatorService $service;
    private string $tempDir;

    // Set up before each test
    protected function setUp(): void
    {
        $this->service = new ZipCreatorService();
        // Create a temporary directory to store test files and the test zip archive
        $this->tempDir = sys_get_temp_dir() . '/zip_test';
        (new Filesystem())->mkdir($this->tempDir);
    }

    // Clean up after each test
    protected function tearDown(): void
    {
        // Remove the temporary directory and all its contents
        (new Filesystem())->remove($this->tempDir);
    }

    // Test that a ZIP archive can be successfully created with a specified file
    public function testCreateZipArchiveSuccess()
    {
        // Create a test file in the temporary directory
        $filePath = $this->tempDir . '/test.txt';
        file_put_contents($filePath, 'Hello World');

        // Specify the path for the ZIP archive to be created
        $zipPath = $this->tempDir . '/test.zip';

        // Attempt to create the ZIP archive containing the test file
        $this->service->createZipArchive(['test.txt' => $filePath], $zipPath);

        // Assert that the ZIP archive was successfully created
        $this->assertFileExists($zipPath);

        // Open the created ZIP archive and check if the test file is included
        $zip = new \ZipArchive();
        $zip->open($zipPath);
        $this->assertTrue($zip->locateName('test.txt') !== false);
        $zip->close();
    }

    // Test that an exception is thrown when attempting to create a ZIP archive with a nonexistent file
    public function testCreateZipArchiveWithNonexistentFileThrowsException()
    {
        // Expect an exception to be thrown during this test
        $this->expectException(\Exception::class);

        // Specify a path for a nonexistent file
        $nonexistentFilePath = $this->tempDir . '/nonexistent.txt';
        // Specify the path for the ZIP archive that should not be created due to the error
        $zipPath = $this->tempDir . '/shouldNotBeCreated.zip';

        // Attempt to create the ZIP archive, expecting the process to fail and throw an exception
        $this->service->createZipArchive(['nonexistent.txt' => $nonexistentFilePath], $zipPath);
    }
}