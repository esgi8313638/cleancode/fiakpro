<?php

namespace App\Tests\Unit\Services;

use App\Application\Exceptions\ExcelDataInjectionException;
use App\Application\Exceptions\ExcelTemplateLoadException;
use App\Application\Services\ExcelGeneratorService;
use App\Application\Services\FileSystem;
use App\Application\Services\IIOFactoryWrapper;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;

/**
 * Unit tests for ExcelGeneratorService.
 *
 * This test class aims to validate the behavior of the ExcelGeneratorService,
 * especially the creation of Excel spreadsheets from provided data
 * and the use of Excel templates.
 */
class ExcelGeneratorServiceTest extends TestCase
{
    private IIOFactoryWrapper $mockIOFactoryWrapper;
    private FileSystem $mockFileSystem;
    private ExcelGeneratorService $service;

    /**
     * Initial setup for each test.
     *
     * Initializes the necessary mocks for the service's dependencies
     * and injects these mocks into a new instance of ExcelGeneratorService.
     */
    protected function setUp(): void {
        parent::setUp();

        // Mock of IIOFactoryWrapper to simulate loading Excel sheets.
        $this->mockIOFactoryWrapper = $this->createMock(IIOFactoryWrapper::class);

        // Mock of FileSystem to simulate file system checks.
        $this->mockFileSystem = $this->createMock(FileSystem::class);

        // Creation of the service instance to be tested with the mocks and a silent logger.
        $this->service = new ExcelGeneratorService($this->mockIOFactoryWrapper, $this->mockFileSystem);
        $this->service->setLogger(new NullLogger());
    }

    /**
     * Tests the creation of a spreadsheet without using a template.
     *
     * Validates that the service can create an Excel spreadsheet based solely
     * on the provided data, without loading an external template.
     */
    public function testCreateExcelSpreadsheetWithoutTemplate()
    {
        $data = [[1, 'two', 3.0]];

        // Configuration of the mock to return a new Spreadsheet object.
        $this->mockIOFactoryWrapper->method('load')->willReturn(new Spreadsheet());

        // Execution of the method to be tested with the provided data.
        $spreadsheet = $this->service->createExcelSpreadsheet($data);

        // Assertions to verify the expected result.
        $this->assertInstanceOf(Spreadsheet::class, $spreadsheet);
        $this->assertEquals('1', $spreadsheet->getActiveSheet()->getCell('A2')->getValue());
        $this->assertEquals('two', $spreadsheet->getActiveSheet()->getCell('B2')->getValue());
        $this->assertEquals(3.0, $spreadsheet->getActiveSheet()->getCell('C2')->getValue());
        $this->assertEquals(2, $spreadsheet->getActiveSheet()->getHighestRow());
        $this->assertEquals('C', $spreadsheet->getActiveSheet()->getHighestColumn());
    }

    /**
     * Tests the creation of a spreadsheet using a template.
     *
     * Validates that the service can load an Excel template and inject the provided data.
     */
    public function testCreateExcelSpreadsheetWithTemplate()
    {
        $data = [[7, 8, 9], [10, 11, 12]];
        $templatePath = 'path/to/template.xlsx';

        // Configures the mock to simulate the successful loading of an Excel template.
        $this->mockIOFactoryWrapper->expects($this->once())
            ->method('load')
            ->with($this->equalTo($templatePath))
            ->willReturn(new Spreadsheet());

        // Simulates a positive file check response.
        $this->mockFileSystem->expects($this->once())
            ->method('fileExistsAndReadable')
            ->with($this->equalTo($templatePath))
            ->willReturn(true);

        // Execution and verification.
        $spreadsheet = $this->service->createExcelSpreadsheet($data, $templatePath);
        $this->assertInstanceOf(Spreadsheet::class, $spreadsheet);
    }

    /**
     * Tests the service behavior when the template path is invalid.
     *
     * Validates that the service throws an ExcelTemplateLoadException if the specified
     * template does not exist or is not readable.
     */
    public function testCreateExcelSpreadsheetWithInvalidTemplatePath()
    {
        $this->expectException(ExcelTemplateLoadException::class);

        $templatePath = 'chemin/invalide/template.xlsx';
        $data = [[1, 2, 3]];

        // Simulates the failure of the file model existence check.
        $this->mockFileSystem->expects($this->once())
            ->method('fileExistsAndReadable')
            ->with($this->equalTo($templatePath))
            ->willReturn(false);

        // Attempts to create a spreadsheet with an invalid template path.
        // The ExcelTemplateLoadException is expected.
        $this->service->createExcelSpreadsheet($data, $templatePath);
    }

    /**
     * Tests the creation of a spreadsheet with empty data.
     *
     * Validates that the service throws an ExcelDataInjectionException if the data provided
     * for generating the Excel spreadsheet is empty.
     */
    public function testCreateExcelSpreadsheetWithEmptyData()
    {
        $this->expectException(ExcelDataInjectionException::class);

        // Attempts to create a spreadsheet without providing any data.
        // An ExcelDataInjectionException is expected.
        $this->service->createExcelSpreadsheet([], 'path/to/template.xlsx');
    }
}