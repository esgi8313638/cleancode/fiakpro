<?php

namespace App\Tests\Functional\APIPlatform;

use App\Domain\Entities\Client;

class ClientTest extends Authentication
{
    public function testGetCollection(): void
    {
        // The client implements Symfony HttpClient's `HttpClientInterface`, and the response `ResponseInterface`
        $response = $this->createClientWithCredentials()->request('GET', '/api/clients');

        $this->assertResponseStatusCodeSame(200);
        // Asserts that the returned content type is JSON-LD (the default)
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

        // Asserts that the returned JSON is a superset of this one
        $this->assertJsonContains([
            "@context" => "/api/contexts/Client",
            "@id" => "/api/clients",
            "@type" => "hydra:Collection",
            "hydra:totalItems" => 3
        ]);

        $this->assertCount(3, $response->toArray()['hydra:member']);
    }

    public function testPost(): void
    {
        $name = strval(rand());
        $this->createClientWithCredentials()->request('POST', '/api/clients', ['headers' => ['Content-Type' => 'application/ld+json'], 'json' => [
            "nom" => $name,
            "adresse" => "14 rue des champignons",
            "telephone" => "0733717075",
            "email" => "test@test.fr",
        ]]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            "@context" => "/api/contexts/Client",
            "@type" => "Client",
            "nom" => $name,
            "adresse" => "14 rue des champignons",
            "telephone" => "0733717075",
            "email" => "test@test.fr",
        ]);
    }

    public function testPostInvalid(): void
    {
        $name = strval(rand());
        $this->createClientWithCredentials()->request('POST', '/api/clients', ['headers' => ['Content-Type' => 'application/ld+json'], 'json' => [
            "nom" => $name,
            "adresse" => "14 rue des champignons",
            "telephone" => 0733717075,
            "email" => "test@test.fr",
        ]]);

        $this->assertResponseStatusCodeSame(400);
        $this->assertResponseHeaderSame('content-type', 'application/problem+json; charset=utf-8');
    }


    // Test if user is not logged in
    public function testUnauthorized() {
        static::createClient()->request('GET', '/api/clients');

        $this->assertResponseStatusCodeSame(401);
        $this->assertJsonContains(['code' => 401, 'message' => 'JWT Token not found']);
    }
}

